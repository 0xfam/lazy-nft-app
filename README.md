# LAZY NFT APP
> A Rarible Lazy Minting App

## How To Start
- Without Docker
```
npm run dev

# or

yarn dev
```

- With Docker
```
docker compose up
```