import { useEffect, useState, useRef } from 'react';
import oxsis from 'lib/oxsis';
import { updateGas } from 'actions';
import { connect } from 'react-redux';
function GasWidget({
  updateGas = () => {
    return;
  },
  address,
}) {
  const [state, setState] = useState({
    lastBlock: null,
    slow: null,
    price: null,
    standard: null,
    fast: null,
    rapid: null,
    show: false,
  });
  const [gas_view, setGasView] = useState('fast');
  const [num, setNum] = useState(8);

  let intervalRef = useRef(null);

  const decreaseNum = () => setNum((prev) => prev - 1);

  useEffect(() => {
    console.log('init gas station');
    if (updateGas !== undefined) {
      oxsis.getGasFees((datas) => {
        setState({ ...datas });
        updateGas(datas);
      });
    }
    // eslint-disable-next-line
  }, []); // eslint-disable-next-line
  // Added [] as useEffect filter so it will be executed only once, when component is mounted
  return (
    <>
      <style jsx>
        {`
          .gas-item {
            min-width: 50px;
          }
          .cursor-pointer {
            cursor: pointer;
          }
          .active {
            background-color: #e6e6fa;
          }
          .loader {
            position: fixed; /* Sit on top of the page content */
            top: 0;
            left: 0;
            background-color: grey;
            z-index: 100; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
            opacity: 50%;
          }
          .loader div {
            opacity: 100%;
          }
          a{ none; color:#000;
        `}
      </style>
      {/* <hr /> */}
      {state.show ? (
        <>
          <div className="h1">
            Lazy NFT{' '}
            <a
              rel="noreferrer"
              href="https://ethereum.org/en/developers/docs/gas/"
              target="_blank"
              title="Link To Gas Fee Documentation Provided By Ethereum.org">
              /Gas
            </a>
          </div>
          <p className={`h4`}>Base Gas Fees</p>(Change Base Gas Fees to Update
          Prices Below)
          <br />
          <span>Update: ~{num}sec</span>
          <p>Block Number: #{state.lastBlock}</p>
          <hr />
          <div className={`gas-widget d-flex flex-row flex-wrap col-12 my-3`}>
            <div
              onClick={
                () => {
                  setGasView('rapid');
                } /* eslint-disable-line react/jsx-no-bind */
              }
              className={`card cursor-pointer  border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center ${
                gas_view === 'rapid' && 'active'
              }`}>
              <h1>{`${state.rapid}`}</h1>
              Rapid Gas Price
              <br />
              {((state.rapid / 1000000000) * 21000)
                .toString()
                .substring(0, 9)}{' '}
              ETH
              <br />$
              {((state.rapid / 1000000000) * 21000 * state.price)
                .toString()
                .substring(0, 4)}
              {' USD'} | 15 Seconds
            </div>
            <div
              onClick={
                () => {
                  setGasView('fast');
                } /* eslint-disable-line react/jsx-no-bind */
              }
              className={`card cursor-pointer border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center ${
                gas_view === 'fast' && 'active'
              }`}>
              <h1>{`${state.fast}`}</h1>
              Fast Gas Price
              <br />
              {((state.fast / 1000000000) * 21000)
                .toString()
                .substring(0, 9)}{' '}
              ETH
              <br />$
              {((state.fast / 1000000000) * 21000 * state.price)
                .toString()
                .substring(0, 4)}
              {' USD'} | 1 Minute
            </div>
            <div
              onClick={
                () => {
                  setGasView('standard');
                } /* eslint-disable-line react/jsx-no-bind */
              }
              className={`card cursor-pointer border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center ${
                gas_view === 'standard' && 'active'
              }`}>
              <h1>{`${state.standard}`}</h1>
              Standard Gas Price
              <br />
              {((state.standard / 1000000000) * 21000)
                .toString()
                .substring(0, 9)}{' '}
              ETH
              <br />$
              {((state.standard / 1000000000) * 21000 * state.price)
                .toString()
                .substring(0, 4)}
              {' USD'} | 5 Minutes
            </div>
            <div
              onClick={
                () => {
                  setGasView('slow');
                } /* eslint-disable-line react/jsx-no-bind */
              }
              className={`card cursor-pointer border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center ${
                gas_view === 'slow' && 'active'
              }`}>
              <h1>{`${state.slow}`}</h1>
              Slow Gas Price
              <br />
              {((state.slow / 1000000000) * 21000)
                .toString()
                .substring(0, 9)}{' '}
              ETH
              <br />$
              {((state.slow / 1000000000) * 21000 * state.price)
                .toString()
                .substring(0, 4)}
              {' USD'} | {'>'} 10 Minutes
            </div>
          </div>
          <br />
          <p className={`h4`}>Opensea Gas Fees</p>
          <hr />
          <div className={`gas-widget d-flex flex-row flex-wrap col-12 my-3`}>
            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Register Account</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              389335 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 389335)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              389335 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 389335)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              389335 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 389335)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              389335 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 389335)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Mint</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) * 0 * state.price * 100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 0)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) * 0 * state.price * 100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 0)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              0 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 0)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) * 0 * state.price * 100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 0)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Buy</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              189873 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 189873)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              189873 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 189873)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              189873 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 189873)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              189873 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 189873)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Cancel Sell</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              74468 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 74468)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              74468 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 74468)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              74468 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 74468)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              74468 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 74468)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Transfer</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              123826 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 123826)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              123826 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 123826)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              123826 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 123826)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              123826 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 123826)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Approve Token</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              46485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 46485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              46485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 46485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              46485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 46485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              46485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 46485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Deposit ETH To Poylgon</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              141736 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 141736)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              141736 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 141736)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              141736 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 141736)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              141736 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 141736)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>
          </div>
          <br />
          <p className={`h4`}>Rarible Gas Fees</p>
          <hr />
          <div className={`gas-widget d-flex flex-row flex-wrap col-12 my-3`}>
            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Mint</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              314252 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 314252)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              314252 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 314252)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              314252 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 314252)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              314252 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 314252)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>
            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Buy</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              317612 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 317612)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              317612 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 317612)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              317612 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 317612)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              317612 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 317612)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Transfer</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              32485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 32485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              32485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 32485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              32485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 32485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              32485 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 32485)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Approve</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Bid</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              46453 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 46453)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>
            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Burn</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              30651 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 30651)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              30651 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 30651)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              30651 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 30651)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              30651 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 30651)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Cancel Sell</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              79175 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 79175)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              79175 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 79175)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              79175 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 79175)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              79175 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 79175)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Create ERC-721 Collection</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              3194260 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 3194260)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              3194260 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 3194260)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              3194260 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 3194260)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              3194260 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 3194260)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>
            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Create ERC-1155 Collection</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              2937929 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 2937929)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              2937929 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 2937929)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              2937929 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 2937929)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              2937929 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 2937929)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Claim Rari Tokens</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              229755 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 229755)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              229755 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 229755)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              229755 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 229755)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              229755 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 229755)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>
          </div>
          <br />
          <p className={`h4`}>Zora Gas Fees</p>
          <hr />
          <div className={`gas-widget d-flex flex-row flex-wrap col-12 my-3`}>
            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Mint</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              657280 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 657280)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              657280 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 657280)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              657280 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 657280)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              657280 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 657280)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>

            <div
              className={`card border border-dark p-3 gas-item col-12 col-md-6 col-lg-3 text-center`}>
              <p className={`h4`}>Approve Token</p>
              {(() => {
                switch (gas_view) {
                  case 'rapid':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.rapid / 1000000000) *
                              51000 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.rapid / 1000000000) * 51000)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'fast':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.fast / 1000000000) *
                              51000 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.fast / 1000000000) * 51000)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                        <br />
                      </>
                    );
                  case 'standard':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.standard / 1000000000) *
                              51000 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.standard / 1000000000) * 51000)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  case 'slow':
                    return (
                      <>
                        <p className={`h2`}>
                          $
                          {Math.ceil(
                            (state.slow / 1000000000) *
                              51000 *
                              state.price *
                              100
                          ) / 100}
                          {' USD'}
                        </p>
                        {((state.slow / 1000000000) * 51000)
                          .toString()
                          .substring(0, 9)}{' '}
                        ETH
                      </>
                    );
                  default:
                    return <div></div>;
                }
              })()}
            </div>
          </div>
          <br />
          <p className={`h4`}>Foundation Gas Fees</p>
          <hr />
          Coming Soon
          <hr />
        </>
      ) : (
        <div
          className={`w-100 h-100 loader d-flex flex-column justify-content-center align-items-center`}>
          <div className="mx-auto text-uppercase mb-3">
            Loading Gas Station
            <hr />
          </div>
          <div className="spinner-border mx-auto" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      )}
    </>
  );
}
const mapStateToProps = (state) => ({
  address: state.session.address,
});
export default connect(mapStateToProps, { updateGas })(GasWidget);
