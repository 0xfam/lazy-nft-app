import { useRef, useEffect, useState } from 'react';
import NFTInput from './NFTInput';
import Button from './common/button';
import { connect } from 'react-redux';
import Input from './common/input';
import { MoiNFTs } from '../lib';
import { _metadata, _metadataTypes } from 'lib/metadataSchema.ts';
import { event } from 'utility/analytics';
import { truncateAddress } from 'lib/moiWeb3';
import FormInputs from './FormInputs';
import Modal from './common/modal';

const moiNFTs = new MoiNFTs({
  _host: process.env.RAREPRESS + '/' + process.env.RAREPRESS_VERSION,
  _storeAccount: process.env.WALLET_ADDRESS,
});

function NFTForm({ address }) {
  const [state, setState] = useState({
    owners: [
      {
        address: address,
        value: 100,
      },
    ],
    ..._metadata,
    supply: 1,
    price: 0,
    type: '',
    royalties: 0,
    attributes: [
      {
        trait_type: 'Minted With:',
        value: 'THE LAZY NFT APP - https://lazynft.app',
      },
      { trait_type: 'Lazy Mint Date:', value: new Date().toUTCString() },
    ],
    token: null,
    disable: true,
    showInput: false,
    isLoading: false,
  });

  const handleFormResponses = (e, data) => {
    if (
      _metadataTypes[data + 'Type'] == 'string' ||
      _metadataTypes[data + 'Type'] == 'url' ||
      _metadataTypes[data + 'Type'] == 'color'
    ) {
      setState({ ...state, [data]: e.target.value });
    }
  };

  return (
    <div
      className={
        'nft-mint-form d-flex flex-column m-1 pb-5 mx-auto container-fluid h-auto'
      }>
      <style global jsx>
        {`
          .file-widget {
            max-width: 18.75rem;
            max-width: 37.5rem;
          }
          .royalty-btn {
            min-width: 4.6875rem;
            margin: 0.5rem;
          }
          .nft-img-preview img {
            object-fit: contain;
          }
          .nft-img-preview img,
          .nft-video-preview video {
            max-height: 300px;
          }
          .loader {
            position: fixed; /* Sit on top of the page content */
            top: 0;
            left: 0;
            background-color: grey;
            z-index: 100; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
            opacity: 50%;
          }
          .loader div {
            opacity: 100%;
          }
        `}
      </style>

      <div className="rounded overflow-md-scroll d-flex flex-column justify-content-between align-items-center p-1 h-100">
        <h4 className="ps-3">
          LAZY MINT YOUR NFT TO{' '}
          <a
            href={`https://${
              process.env.NODE_ENV !== 'production' && 'rinkeby.'
            }`}>
            {process.env.NODE_ENV !== 'production' && 'RINKEBY.'}RARIBLE.COM
          </a>
        </h4>
        {/* TOP SECTION */}
        <div className="d-flex flex-xl-row flex-column flex-wrap justify-content-between mx-2">
          <div
            className={`col-xl-3 m-3 p-2 border border-dark d-inline-flex flex-column`}>
            <h4>NFT INFO</h4>
            <hr />
            {Object.keys(_metadata).map((data, key) => (
              <FormInputs
                show={data === 'image' || state.showInput}
                key={key}
                id={data}
                label={data.replace('_', ' ')}
                type={_metadataTypes[data + 'Type']}
                onChange={(e) => handleFormResponses(e, data)}
                style={`w-100`}
              />
            ))}
          </div>

          <div
            className={`col-xl-5 m-3 p-2 border border-dark d-inline-flex flex-column`}>
            <NFTInput
              id={'nft-input'}
              label={'File:'}
              onChange={(e) => {
                if (typeof e.fileType !== 'undefined') {
                  e.fileType.split('/')[0] === 'audio' ||
                  e.fileType.split('/')[0] === 'video'
                    ? setState({
                        ...state,
                        animation_url: '/ipfs/' + e.cid,
                        fileData: '',
                        type: e.fileType.split('/')[0],
                        showInput: true,
                        token: null,
                      })
                    : setState({
                        ...state,
                        animation_url: '',
                        fileData: '/ipfs/' + e.cid,
                        type: e.fileType.split('/')[0],
                        disable: false,
                        token: null,
                        showInput: false,
                      });
                } else {
                  setState({
                    ...state,
                    animation_url: '',
                    fileData: '',
                    type: '',
                    disable: true,
                    showInput: false,
                  });
                }
              }}
            />
            <br />
            {state.showInput && (
              <NFTInput
                id={'nft-input-cover'}
                label={'Cover:'}
                accept={'image/*'}
                onChange={(e) => {
                  if (typeof e.fileType !== 'undefined') {
                    setState({
                      ...state,
                      fileData: '/ipfs/' + e.cid,
                      disable: false,
                      token: null,
                    });
                  } else {
                    setState({
                      ...state,
                      fileData: '',
                      disable: true,
                      showInput: false,
                    });
                  }
                }}
              />
            )}
            <hr />
            <div className={`d-flex flex-column w-auto`}>
              {state.fileData !== undefined && state.fileData.length > 0 && (
                <>
                  <div className={'nft-img-preview mx-auto w-auto'}>
                    <img
                      alt="Lazy NFT App Image"
                      className={'w-100'}
                      src={process.env.RAREPRESS + state.fileData}
                    />
                  </div>
                  <hr />
                </>
              )}
              {state.type.split('/')[0] === 'video' && (
                <div className={'nft-video-preview mx-auto w-auto'}>
                  <video
                    controls
                    className={''}
                    src={process.env.RAREPRESS + state.animation_url}
                  />
                </div>
              )}
              {state.type.split('/')[0] === 'audio' && (
                <div className={'nft-audio-preview'}>
                  <audio
                    controls
                    className={'w-100'}
                    src={process.env.RAREPRESS + state.animation_url}
                  />
                </div>
              )}
            </div>
          </div>
          <div
            className={`col-xl-3 m-3 p-2 border border-dark d-inline-flex flex-column`}>
            <Input
              id="supply"
              value={state.supply}
              placeholder={state.supply}
              label={'Supply:'}
              type="number"
              onChange={(e) => {
                if (e.target.value >= 0) {
                  e.target.value.length !== 0 &&
                    setState({ ...state, supply: e.target.value });
                }
              }}
              min={'0'}
            />
            <hr />
            <Input
              id="price"
              value={state.price}
              placeholder={state.price}
              label={'List Price: (ETH)'}
              type="number"
              onChange={(e) => {
                if (e.target.value >= 0) {
                  e.target.value.length !== 0 &&
                    setState({ ...state, price: e.target.value });
                }
              }}
              min={'0'}
            />
            <hr />
            {/* ROYALTIES */}
            <div className={'d-flex flex-column justfy-content-between'}>
              <p>Royalties:</p>
              <div
                className={` d-flex flex-row flex-wrap justify-content-around`}>
                <Button
                  inputStyle={'btn'}
                  id="royalties-0"
                  value={state.royalties}
                  placeholder={state.royalties + '%'}
                  buttonStyle={`btn-outline-dark royalty-btn ${
                    state.royalties === 0 && 'active'
                  }`}
                  onPress={(e) => {
                    setState({ ...state, royalties: 0 });
                  }}>
                  NONE
                </Button>
                <Button
                  inputStyle={'btn'}
                  id="royalties-2_5"
                  value={state.royalties}
                  placeholder={state.royalties + '%'}
                  buttonStyle={`btn-outline-dark royalty-btn ${
                    state.royalties === 250 && 'active'
                  }`}
                  onPress={(e) => {
                    setState({ ...state, royalties: 250 });
                  }}>
                  2.5%
                </Button>
                <Button
                  inputStyle={'btn'}
                  id="royalties-5"
                  value={state.royalties}
                  placeholder={state.royalties + '%'}
                  buttonStyle={`btn-outline-dark royalty-btn ${
                    state.royalties === 500 && 'active'
                  }`}
                  onPress={(e) => {
                    setState({ ...state, royalties: 500 });
                  }}>
                  5%
                </Button>
                <Button
                  inputStyle={'btn'}
                  id="royalties-10"
                  value={state.royalties}
                  placeholder={state.royalties + '%'}
                  buttonStyle={`btn-outline-dark royalty-btn ${
                    state.royalties === 1000 && 'active'
                  }`}
                  onPress={(e) => {
                    setState({ ...state, royalties: 1000 });
                  }}>
                  10%
                </Button>
                <Button
                  inputStyle={'btn'}
                  id="royalties-15"
                  value={state.royalties}
                  placeholder={state.royalties + '%'}
                  buttonStyle={`btn-outline-dark royalty-btn ${
                    state.royalties === 1500 && 'active'
                  }`}
                  onPress={(e) => {
                    setState({ ...state, royalties: 1500 });
                  }}>
                  15%
                </Button>
                <Button
                  inputStyle={'btn'}
                  id="royalties-25"
                  value={state.royalties}
                  placeholder={state.royalties + '%'}
                  buttonStyle={`btn-outline-dark royalty-btn ${
                    state.royalties === 2500 && 'active'
                  }`}
                  onPress={(e) => {
                    setState({ ...state, royalties: 2500 });
                  }}>
                  25%
                </Button>
              </div>
            </div>
            <hr />
            {
              <Button
                disabled={
                  state.fileData === undefined && state.name.length === 0
                }
                buttonStyle={`btn-dark mb-3`}
                onPress={async () => {
                  await setState({ ...state, isLoading: true });
                  let token = await moiNFTs
                    .mintNFT({
                      account: address,
                      metadata: {
                        name: state.name,
                        description: state.description,
                        image: state.fileData,
                        animation_url: state.animation_url,
                        background_color: state.background_color,
                        external_url: state.external_url,
                        youtube_url: state.youtube_url,
                        attributes: [
                          ...state.attributes,
                          {
                            trait_type: 'File Type:',
                            value: state.type.toUpperCase(),
                          },
                        ],
                        properties: state.properties,
                      },
                      supply: state.supply,
                      _royalties: state.royalties,
                    })
                    .catch(() => {
                      setState({
                        ...state,
                        isLoading: false,
                      });
                    });
                  if (token !== undefined) {
                    event({
                      action: 'mint',
                      params: {
                        event_category: 'mint',
                        event_label: 'mint',
                      },
                    });

                    setState({
                      ...state,
                      token: token.token,
                      disable: !state.disable,
                      isLoading: false,
                    });
                  }
                }}>
                Mint
              </Button>
            }
            {state.isLoading && (
              <div
                className={`w-100 h-100 loader d-flex flex-column justify-content-center align-items-center`}>
                <div className="mx-auto text-uppercase mb-3">
                  Lazy Minting NFT
                  <hr />
                </div>
                <div className="spinner-border mx-auto" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              </div>
            )}
            {state.price !== undefined && state.price > 0 && (
              <Button
                disabled={state.disable && state.name.length !== 0}
                buttonStyle={`btn-dark`}
                onPress={async () => {
                  await setState({ ...state, isLoading: true });
                  let token = await moiNFTs
                    .mintNFT({
                      account: address,
                      metadata: {
                        name: state.name,
                        description: state.description,
                        image: state.fileData,
                        animation_url: state.animation_url,
                        background_color: state.background_color,
                        external_url: state.external_url,
                        youtube_url: state.youtube_url,
                        attributes: [
                          ...state.attributes,
                          {
                            trait_type: 'File Type:',
                            value: state.type.toUpperCase(),
                          },
                        ],
                        properties: state.properties,
                      },
                      supply: state.supply,
                      _royalties: state.royalties,
                    })
                    .catch(() => {
                      setState({
                        ...state,
                        isLoading: false,
                      });
                    });
                  if (token !== undefined) {
                    console.log(token.token);
                    await moiNFTs
                      .tradeNFT({
                        token: token.token,
                        supply: state.supply,
                        _royalties: state.royalties,
                        price: state.price,
                      })
                      .catch(() => {
                        setState({
                          ...state,
                          isLoading: false,
                        });
                      });

                    event({
                      action: 'mint_and_list',
                      params: {
                        event_category: 'mint_and_list',
                        event_label: 'mint_and_list',
                      },
                    });
                    setState({
                      ...state,
                      token: token.token,
                      disable: !state.disable,
                      isLoading: false,
                    });
                  }
                }}>
                Mint/Sell
              </Button>
            )}
          </div>
        </div>

        {/* MINT BUTTONS */}
        <div className={`d-inline-flex flex-column m-3 p-2 border border-dark`}>
          {state.token !== null && (
            <>
              <a
                rel="noreferrer"
                className="btn btn-outline-dark"
                target="_blank"
                href={`https://twitter.com/intent/tweet?text=Minted%20My%20Token:%20${state.name.replace(
                  ' ',
                  '%20'
                )}%20https://${
                  process.env.NODE_ENV !== 'production' ? 'rinkeby.' : ''
                }rarible.com/token/${
                  state.token.id
                }%20via%20%23lazynftapp%20%23nft%20%23nftart%20%40lazynftapp`}>
                Share NFT to Twitter
              </a>
            </>
          )}
          {state.token !== null && <br />}
          {state.token !== null && (
            <a
              rel="noreferrer"
              className="btn btn-outline-dark"
              target="_blank"
              href={`https://${
                process.env.NODE_ENV !== 'production' ? 'rinkeby.' : ''
              }rarible.com/token/${state.token.id}`}>
              View NFT
            </a>
          )}
        </div>
      </div>

      {false && <Modal></Modal>}
    </div>
  );
}
const mapStateToProps = (state) => ({
  address: state.session.address,
});
export default connect(mapStateToProps)(NFTForm);
