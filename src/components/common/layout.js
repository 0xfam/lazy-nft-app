import { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Navbar from './navbar';
import LoginModal from '../loginModal';
import { startCore, updateOnlineUserCount } from '../../actions/';
import { useRouter } from 'next/router';
function Layout({
  children,
  layoutContainerStyle = '',
  startCore,
  updateOnlineUserCount,
  online_user_count,
}) {
  const router = useRouter();
  const [show, setShow] = useState(false);
  const [showSidebar, setShowSidebar] = useState(false);
  useEffect(() => {
    setShowSidebar(window.location.pathname !== '/');
  }, [router.pathname]);
  // INITS WEB3 Functionality
  useEffect(() => {
    startCore();
    // fetch('/api/socketio')
    //   .finally(() => {
    //     const socket = io();

    //     socket.on('connect', (msg) => {
    //       socket.emit('user_connected');
    //     });
    //     socket.on('user_connected', (data) => {
    //       updateOnlineUserCount(data);
    //     });
    //     socket.on('user_disconnected', (data) => {
    //       updateOnlineUserCount(data);
    //     });
    //     socket.on('disconnect', () => {
    //       socket.emit('user_disconnected');
    //     });
    //   })
    //   .catch((error) => {
    //     throw error;
    //   });
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className={`d-flex flex-column h-100 w-100 fnt-monospace`}>
      <style jsx global>
        {`
          html,
          body,
          #__next {
            height: 100%;
            font-family: monospace;
          }

          .content-wrapper {
            height: calc(100% - 36px);
            width: 100%;
          }
        `}
      </style>
      {/* NAVBAR */}
      <Navbar
        navbarContainerStyle={'sticky-top border-top border-bottom border-dark'}
        brandText={process.env.BRAND_NAME || 'LAZYNFT.APP'}
        brandTextStyle={`text-uppercase`}
        onClick={() => setShow(!show)}
        onClickCreate={() => router.push('/mint')}
      />
      <div className={`content-wrapper contianer-fluid d-flex flex-column`}>
        {/**Main Content */}
        <main className={`p-2 w-100 h-100 ${layoutContainerStyle}`}>
          {children}
        </main>
      </div>
      {/* LOGIN MODAL */}
      {show && <LoginModal handleShow={() => setShow(!show)} />}
      <a href="#" id="open_preferences_center" className={'d-none'}>
        Open Preferences Center
      </a>
    </div>
  );
}
const mapStateToProps = (state) => ({
  online_user_count: state.session.online_user_count,
});
export default connect(mapStateToProps, { startCore, updateOnlineUserCount })(
  Layout
);
