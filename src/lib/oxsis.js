import { ethers } from 'ethers';
import detectEthereumProvider from '@metamask/detect-provider';
import { NFTStorage, File } from 'nft.storage';

import moifetch from 'moifetch';
const apiKey = process.env.LAZY_NFT_KEY;
const client = new NFTStorage({ token: apiKey });

let web3;
(async () =>
  (await detectEthereumProvider())
    ? (web3 = new ethers.providers.Web3Provider(window.ethereum))
    : (web3 = new ethers.providers.JsonRpcProvider(process.env.API_URL)))();
// nonce starts counting from 0

export default class oxsis {
  // Foramt
  static formatUnits = async (value, formatTo) => {
    return ethers.utils.formatUnits(value, formatTo);
  };
  static parseUnits = async (value, formatTo) => {
    return ethers.utils.parseUnits(value, formatTo);
  };
  //Get Nonce
  static getNonce = async (address) => {
    try {
      const nonce = await web3.getTransactionCount(address, 'latest');
      console.log('The latest nonce is ' + nonce);
      return await nonce;
    } catch (error) {
      throw error;
    }
  };
  // Get Network
  static getNetwork = async (name_or_id) => {
    try {
      const network = await web3.getNetwork(name_or_id);
      console.log('The network is: ' + JSON.stringify(network));
      return await JSON.stringify(network);
    } catch (error) {
      throw error;
    }
  };
  // Get Block Number
  static getBlockNumber = async () => {
    try {
      const blockNumber = await web3.getBlockNumber();
      console.log('The latest block number is ' + blockNumber);
      return await blockNumber;
    } catch (error) {
      throw error;
    }
  };
  // Get Balance of Address in BigNumber
  static getBalance = async (address) => await web3.getBalance(address);
  //
  static getGasPrice = async () => await web3.getGasPrice();
  //
  static getContract = (contractJson, contractAddress) => {
    // const contractAddress = process.env.CONTRACT_ADDRESS;
    const contract = contractJson;
    return { contract, contractAddress };
  };

  static genMetaData = async (file) => {
    const metadata = await client.store({
      name: file[0].name,
      description: '',
      image: new File(file, file[0].name, { type: file[0].type }),
    });
    return {
      metadata,
      url: metadata.url,
    };
  };

  static signTransaction = async (transaction) => {
    const signedTx = await web3.eth.accounts.signTransaction(transaction);
    return signedTx;
  };

  static sendTransaction = async (transaction) => {
    // const signer = web3.getSigner()
    // const signedTx = await this.signTransaction(transaction);
    const sign = web3.getSigner();
    sign.sendTransaction(transaction, function (error, hash) {
      if (!error) {
        console.log(
          '🎉 The hash of your transaction is: ',
          hash,
          "\n Check Alchemy's Mempool to view the status of your transaction!"
        );
      } else {
        console.log(
          '❗Something went wrong while submitting your transaction:',
          error
        );
      }
    });
  };

  static mintNFT = async (address, tokenURI) => {
    const _web3 = createAlchemyWeb3(
      // window.ethereum
      // 'https://eth-mainnet.alchemyapi.io/v2/5XVWv4P5pthhxbxWAeqgRJ-QpIU9C2uv'
      process.env.API_URL
    );

    const contractData = await this.getContract();
    const nftContract = new _web3.eth.Contract(
      contractData.contract.abi,
      contractData.contractAddress
    );
    console.log(contractData.contract.abi, contractData.contractAddress);
    //the transaction
    const gasPriceLimit = await _web3.eth.estimateGas({
      from: address,
      to: contractData.contractAddress,
      data: nftContract.methods
        .mintNFT(address, 'https://ipfs.io/ipfs/' + tokenURI)
        .encodeABI(),
    });
    const tx = {
      from: address,
      to: contractData.contractAddress,
      gas: gasPriceLimit,
      maxPriorityFeePerGas: 1999999987,
      data: nftContract.methods
        .mintNFT(address, 'https://ipfs.io/ipfs/' + tokenURI)
        .encodeABI(),
    };
    await this.sendTransaction(tx);
  };

  static getGasFees = async (callback) => {
    try {
      let wsObj;
      let wsUrl = 'wss://www.gasnow.org/ws';
      let _data;
      let updatePageGasPriceData = async (data) => {
        const eth = await moifetch.GET(
          'https://api.coingecko.com/api/v3/coins/ethereum'
        );

        if (data && data.gasPrices) {
          let slow = Math.floor(
            await oxsis.formatUnits(
              await oxsis.parseUnits(data.gasPrices.slow.toString(), 'wei'),
              'gwei'
            )
          );
          let standard = Math.floor(
            await oxsis.formatUnits(
              await oxsis.parseUnits(data.gasPrices.standard.toString(), 'wei'),
              'gwei'
            )
          );
          let fast = Math.floor(
            await oxsis.formatUnits(
              await oxsis.parseUnits(data.gasPrices.fast.toString(), 'wei'),
              'gwei'
            )
          );
          let rapid = Math.floor(
            await oxsis.formatUnits(
              await oxsis.parseUnits(data.gasPrices.rapid.toString(), 'wei'),
              'gwei'
            )
          );
          const blockNumber = await this.getBlockNumber();
          _data = {
            lastBlock: blockNumber,
            price: eth.data.market_data.current_price.usd,
            show: true,
            slow: slow,
            standard: standard,
            fast: fast,
            rapid: rapid,
          };
          callback(_data);
        }
      };

      wsObj = new WebSocket(wsUrl);

      wsObj.onopen = (evt) => {
        console.log('Connection open ...');
      };

      wsObj.onmessage = async (evt) => {
        const dataStr = evt.data;
        const data = JSON.parse(dataStr);

        if (data.type) {
          updatePageGasPriceData(data.data);
        }
      };

      wsObj.onclose = (evt) => {
        console.log('Connection closed.');
      };
      return _data;
    } catch (error) {
      throw error;
    }
  };
}
