import { useRef, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Banner from '@/components/common/banner';

import NFTForm from '../components/NFTForm';
import Button from '../components/common/button';
import { useRouter } from 'next/router';
import { event } from 'utility/analytics';
import GasWidget from '@/components/GasWidget';

function Gas({ online_user_count }) {
  const router = useRouter();

  useEffect(() => {
    event({
      action: 'access_gas_station',
    });
  }, []);
  return (
    <div className={'h-100 d-flex flex-column justify-content-between pt-5'}>
      <style jsx>{``}</style>
      {
        <div className={`mx-3`}>
          {/* <p className={`h4`}>Online Users: {online_user_count}</p> */}
          <GasWidget />
        </div>
      }
    </div>
  );
}
const mapStateToProps = (state) => ({
  address: state.session.address,
  online_user_count: state.session.online_user_count,
});
export default connect(mapStateToProps, {})(Gas);
