import { setProfile, connectID, readProfile, login } from 'actions/web3Actions';
import { useRef, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { MoiNFTs, client, getRecord } from '../lib';
import Button from 'components/common/button';
import { useRouter } from 'next/router';
import { event } from 'utility/analytics';
import { truncateAddress } from 'lib/moiWeb3';
const moiNFTs = new MoiNFTs({
  _host: process.env.RAREPRESS + '/' + process.env.RAREPRESS_VERSION,
  _storeAccount: process.env.WALLET_ADDRESS,
});

function Home({
  profile,
  address,
  idxInstance,
  setProfile,
  connectID,
  did,
  readProfile,
  login,
}) {
  const router = useRouter();
  const [bio, setBio] = useState('');
  const [twitter, setTwitter] = useState('');
  const [name, setName] = useState('');
  const [nftArray, setNftArray] = useState([]);
  const [show, setShow] = useState(false);
  const idxRef = useRef(null);
  const didRef = useRef(null);
  idxRef.current = idxInstance;
  didRef.current = did;

  async function updateProfile() {
    if (!twitter && !bio && !name) {
      console.log('error... no profile information submitted');
      return;
    }
    if (!idxInstance) {
      await connectID();
    }
    const user = { ...profile };
    if (twitter) user.twitter = twitter;
    if (bio) user.bio = bio;
    if (name) user.name = name;
    await idxRef.current.set('basicProfile', user);
    setLocalProfileData();
    console.log('profile updated...');
  }

  async function setLocalProfileData() {
    try {
      const data = await idxRef.current.get('basicProfile', didRef.current.id);
      if (!data) return;
      setProfile(data);
    } catch (error) {
      console.log('error', error);
    }
  }

  useEffect(() => {
    async () => {
      address.length !== 0 && readProfile();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [did]);
  useEffect(() => {
    (async () => {
      if (address.length !== 0) {
        const nfts = await moiNFTs.getNFTSByAddress({
          account: address,
        });
        setNftArray(nfts.result);
        console.log('nfts', nfts.result);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [address]);
  return (
    <div className={`d-flex flex-column mx-2`}>
      <style jsx>
        {`
          .profile-form {
            min-width: 200px;
            width: 100%;
            max-width: 43.75rem;
          }
          .nft-card {
            min-width: 200px;
            width: 100%;
            max-width: 400px;
            height: 600px;
          }
          .nft-card p {
            margin: 0;
          }
          .object-fit {
            object-fit: contain;
          }
          .nft-img-container {
            max-width: 300px;
            max-height: 350px;
          }
        `}
      </style>
      {idxInstance && Object.keys(profile).length && (
        <div className={`d-flex  flex-column mx-auto mb-4`}>
          {profile.image !== undefined && (
            <img
              style={{
                width: profile.image.original.width,
                height: profile.image.original.height,
              }}
              src={
                'https://ipfs.io/ipfs/' +
                profile.image.original.src.replace('ipfs://', '')
              }
              alt=""
            />
          )}
          <h2 className="text-2xl font-semibold mt-6">{profile.name}</h2>
          <p className="text-gray-500 text-sm my-1">{profile.bio}</p>
          <p className="text-lg	text-gray-900">
            Follow me on Twitter - @{profile.twitter}
          </p>
        </div>
      )}
      {address && Object.keys(profile).length === 0 && (
        <p className="my-4 font-bold text-center">
          You have no profile yet. Please create one!
        </p>
      )}
      {address && !idxInstance && (
        <div
          className={`d-flex flex-column mx-auto align-items-center justify-content-center`}>
          <Button
            onPress={async () => {
              try {
                await login();
                await event({
                  action: 'login',
                });
              } catch (error) {
                console.log(error);
              }
            }}
            buttonStyle={'border border-dark'}>
            Connect Wallet to Ceramic
          </Button>
          <hr />
        </div>
      )}
      {Object.keys(profile).length === 0 && (
        <div
          className={`d-flex flex-row flex-wrap mx-auto align-items-center justify-content-center`}>
          <Button
            onPress={async () => {
              try {
                await setShow(!show);
                await event({
                  action: 'create_profile',
                });
              } catch (error) {
                console.log(error);
              }
            }}
            buttonStyle={'border border-dark'}>
            Create Profile
          </Button>
          <hr />
        </div>
      )}
      {address && idxInstance && (
        <div
          className={`d-flex flex-row flex-wrap mx-auto align-items-center justify-content-center`}>
          <Button
            onPress={async () => {
              try {
                await setShow(!show);
                await event({
                  action: 'update_profile_toggle',
                });
              } catch (error) {
                console.log(error);
              }
            }}
            buttonStyle={'border-bottom border-dark'}>
            Update Profile
          </Button>
        </div>
      )}
      <hr />
      {show && (
        <div className={`profile-form p-2 m-2 mx-auto`}>
          <input
            className="rounded bg-gray-100 p-3 w-100"
            placeholder="Display Name:"
            onChange={(e) => setName(e.target.value)}
          />
          <hr />
          <input
            className="rounded bg-gray-100 p-3 w-100"
            placeholder="Bio:"
            onChange={(e) => setBio(e.target.value)}
          />
          <hr />
          <input
            className="rounded bg-gray-100 p-3 w-100"
            placeholder="Twitter:"
            onChange={(e) => setTwitter(e.target.value)}
          />{' '}
          <hr />
          <button
            className="btn btn-outline-dark mt-2 mb-2 font-bold w-100"
            onClick={async () => {
              await updateProfile();
              await event({
                action: 'update_profile',
              });
            }}>
            Save
          </button>
          <hr />
        </div>
      )}
      {
        <div
          className={`d-flex flex-column mx-auto align-items-center justify-content-center`}>
          <Button
            buttonStyle={'btn-outline-dark border border-dark'}
            onPress={async () => {
              await event({
                action: 'access_mint_page',
              });
              await router.push('/mint');
            }}>
            Create NFT
          </Button>
          <hr />
        </div>
      }

      {address && idxInstance && (
        <div
          className={`d-flex flex-column flex-wrap mx-auto align-items-center justify-content-center`}>
          <p>{`My Collection`}</p>
          <hr />
          <div
            className={`d-flex flex-row flex-wrap mx-auto align-items-center justify-content-center`}>
            {nftArray
              .filter(
                ({ royalties, creators, supply, contract, tokenId }) =>
                  royalties[0].value !== '1.50'
              )
              .map(
                (
                  {
                    metadata: {
                      animation_url,
                      attributes,
                      background_color,
                      description,
                      external_url,
                      image,
                      name,
                      properties,
                    },
                    royalties,
                    creators,
                    supply,
                    contract,
                    tokenId,
                  },
                  key
                ) => {
                  return (
                    <div
                      key={key}
                      className={`card nft-card m-3 p-4 d-flex flex-column justify-content-between `}>
                      <div className={`p-3 nft-img-container`}>
                        <img
                          className={`w-100 object-fit`}
                          src={'https://ipfs.io/' + image}
                          alt={name}
                        />
                      </div>
                      <div className={`p-3`}>
                        <p>Title: {name}</p>
                        {/* <p>Description: {description}</p> */}
                        <p>Supply: {supply}</p>
                        <p>
                          Royalties:{' '}
                          {royalties.map((royalty, key) => (
                            <span key={key}>
                              {royalty.value === 2500
                                ? '25%'
                                : royalty.value === 1500
                                ? '15%'
                                : royalty.value === 500
                                ? '5%'
                                : royalty.value === 250
                                ? '2.5%'
                                : royalty.value === 0 && '0%'}
                            </span>
                          ))}
                        </p>
                        <p>
                          Creators:{' '}
                          {creators.map((creator, key) => (
                            <span key={key} title={creator.account}>
                              {truncateAddress(creator.account)}
                            </span>
                          ))}
                        </p>
                      </div>
                      <a
                        rel="noreferrer"
                        className="btn btn-outline-dark"
                        target="_blank"
                        href={`https://${
                          process.env.NODE_ENV !== 'production'
                            ? 'rinkeby.'
                            : ''
                        }rarible.com/token/${contract}:${tokenId}`}
                        onClick={async () => {
                          await event({
                            action: 'view_nft_rarible',
                          });
                        }}>
                        View NFT On Rarible
                      </a>
                    </div>
                  );
                }
              )}
          </div>
        </div>
      )}
    </div>
  );
}
const mapStateToProps = (state) => ({
  address: state.session.address,
  profile: state.session.did_profile,
  idxInstance: state.session.idxInstance,
  did: state.session.did,
});
export default connect(mapStateToProps, {
  setProfile,
  connectID,
  readProfile,
  login,
})(Home);
