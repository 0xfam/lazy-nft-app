import { useRef, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Button from '../components/common/button';
import { useRouter } from 'next/router';
import { event } from 'utility/analytics';
import GasWidget from '@/components/GasWidget';
import oxsis from 'lib/oxsis';

function _Index({ address }) {
  const router = useRouter();
  useEffect(() => {
    async function main() {
      await oxsis.getNonce(address);
      await oxsis.getNetwork();
      await oxsis.getBlockNumber();
      console.log('My Balance', await oxsis.getBalance(address));
      console.log(
        'Gas Price in GWEI',
        await oxsis.formatUnits(await oxsis.getGasPrice(), 'gwei')
      );
    }
    main();
  }, [address]);
  // setInterval(async () => console.log(await oxsis.getFeeHistory()), 8000);
  return (
    <div className={'h-100 d-flex flex-column justify-content-between'}>
      <style jsx>
        {`
          .nft-mint-form {
            max-width: 700px;
          }
        `}
      </style>
      <div className={' pb-5'}>
        <Button
          onPress={async () => {
            event({
              action: 'donate',
            });
            const transaction = {
              to: '0x877728846bFB8332B03ac0769B87262146D777f3',
              value: 0.0001 * Math.pow(10, 18),
            };
            oxsis.sendTransaction(transaction);
          }}>
          Support the Dev
        </Button>
      </div>
      <div className={'home-header mt-5 mx-auto p-5 text-center my-5'}>
        <p className={'display-2 text-center'}>LAZY NFT APP</p>
        <p>
          Easy and Free{' '}
          <a
            rel="noreferrer"
            target="_blank"
            href={`https://bafybeiagihgcydu33mvdigtlneqsvt5beariazpgb3rlzu3yhbv2kuo4au.ipfs.dweb.link/how-to/lazy-minting/#how-it-works`}
            title={'Learn More'}>
            Lazy Minting
          </a>{' '}
          for Creators.
        </p>
        <p>Gas Station Coming Soon</p>
        <Button
          buttonStyle={'btn-outline-dark border border-dark'}
          onPress={async () => {
            event({
              action: 'launch_app',
            });
            await router.push('/mint');
          }}>
          Launch App
        </Button>
        <hr />
        <>
          <p>
            <a
              rel="noreferrer"
              target="_blank"
              className={''}
              href="https://linktr.ee/moikapy">
              Link Tree
            </a>
          </p>
        </>
      </div>
      {
        <div className={`d-flex flex-row justify-content-between mx-3`}>
          {/* <GasWidget /> */}
          <a
            className={`btn btn-outline-dark`}
            href={'https://akkoros.xyz'}
            target={'_blank'}
            rel={'noreferrer'}
            onClick={async () => {
              event({
                action: 'go_to_akkoros',
              });
            }}>
            Visit Our Solana Market
          </a>
        </div>
      }
    </div>
  );
}
const mapStateToProps = (state) => ({
  address: state.session.address,
});
export default connect(mapStateToProps)(_Index);
